# LogFile Upload system

This demo project realizes a monitor that observes a folder containing log files, and whenver a file changes, it 
is uploaded to a server. The demo includes two components

- A server node storing uploaded log files (Upload realized via [Repo Insertion Protocol](https://redmine.named-data.net/projects/repo-ng/wiki/Basic_Repo_Insertion_Protocol))
- The Logging Node (Monitoring a logfile directory and sending the file to the Repo)

## Installation

1) Create activate `virtualenv`

```bash
virtualenv ndnpyenv
source ./ndnpyenv/bin/activate
```

2) Install Python-NDN (Requirement for Repo and Logging node)

```bash
git clone https://github.com/named-data/python-ndn.git
cd python-ndn
python3 setup.py install
cd ..
```

3) Open new Terminal and start NFD

```bash
nfd-start
```

4) Open new Terminal and Install and InsertOnlyBackend of Repo

```bash
source ./ndnpyenv/bin/activate
git clone https://github.com/phylib/ndn-python-repo.git
cd ndn-python-repo
git checkout InsertOnlyBackend
python3 setup.py install
python3 ndn_python_repo/cmd/main.py --config <your repo config file>
```

5) Open new Terminal, checkout and start LogFileMonitor

```bash
source ./ndnpyenv/bin/activate
git clone https://gitlab.com/mollph/logfileuploader.git
cd logfileuploader
mkdir key
python3 src/keygenerator.py
python3 src/FileUploader.py
```

6) Everything works: Files from `log` folder (LogfileMonitor) are observed and changed files uploaded to the `dest`
folder of the server node.
   
## Configuration on multiple machines

When having multiple machines, NFD's on all machines have to be started. In addition, Face and Route configuration
has to be configured.

### Connectivity and Reachabilty
The repo should reach the uploader by its prefix, and the uploader should reach the repo by repo prefx. 
Therefore, the corresponding faces and routes are needed:
For example, on the repo side:
```bash
nfdc route add <uploader prefix> <uploader face>
```
And on the uploader side: 
```bash
nfdc route add <repo prefix> <repo face>
```

### Identity and Security
Before ``FileUploader``, users should run ``keygenerator`` to generate node's identity safebag and content encryption key files in the default directory ``/key``.
Before the repo started, users need to copy the directory from the uploader machine to the repo machine.
The directory should be specifed in the config file of repo.
For example, in the ``security_config`` section of the example config file:
```
security_config:
    dir: '/home/pi/repo-client/key/'
    # directory to key files
```
Upon starting, repo will import the identity safebag into its keychain.